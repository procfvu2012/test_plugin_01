
class AssigneesController < ApplicationController
  unloadable

  before_action :backup_params, :only => [:show] 

  before_action :find_watchables, :only => [:assign, :unassign]

  def backup_params
    if params.has_key?(:current_selected_assignees)
    end
  end

  def assign
    set_assignee(@assignables, User.current, true)
  end

  def unassign
    set_assignee(@assignables, User.current, false)
  end

  # method: index (for testing)

  def index
    project = Project.find(params[:project_id])
    #@assignees = Assignee.all
    @assignees = project.memberships
  end

  before_filter :find_project, :only => [:new, :create, :append, :destroy, :autocomplete_for_user, :show, :users_for_assignees, :filter_assignee]
  accept_api_auth :assign,

  # method: new
  def new
    @users = filter_assignee#users_for_assignees
  end

  def test
    @users = users_for_assignees
  end

  # method: find_project
  def find_project
    if params[:object_type] && params[:object_id]
      klass = Object.const_get(params[:object_type].camelcase)
      return false unless klass.respond_to?('watched_by')
      @watched = klass.find(params[:object_id])
      @project = @watched.project
    elsif params[:project_id]
      @project = Project.visible.find_by_param(params[:project_id])
    end
  rescue
    render_404
  end

  # method: users_for_assignees
  def users_for_assignees
    scope = nil

    if @project.present?
      scope = @project.users
    end

    users = scope.active.visible.sorted.like(params[:q]).to_a

    # if params.has_key?(:project_name)
    #   info                = params[:project_name]
    #   currentproject      = Project.visible.where("identifier = (?)", info).to_a
    #   project_id          = currentproject.first.id

    #   scope ||= User.active.joins(:members).where("#{Member.table_name}.project_id = ?", project_id).uniq
    # else
    #   if @project.present?
    #     scope = @project.users
    #   end
    # end
    # users = scope.active.visible.sorted.like(params[:q]).to_a
    
    if @assigned
      users -= @assigned.assignee_users
    end

    users
  end

  # method append
  def create
    user_ids = []
    if params[:assignee].is_a?(Hash)
      user_ids << (params[:assignee][:user_ids] || params[:assignee][:user_id])
    else
      user_ids << params[:user_id]
    end
    users = User.active.visible.where(:id => user_ids.flatten.compact.uniq)
    users.each do |user|
      Assignee.create(:assignable => @assigned, :user => user)
    end
    respond_to do |format|
      format.html { redirect_to_referer_or {render :text => 'Assignee added.', :layout => true}}
      format.js { @users = users_for_assignees }
      format.api { render_api_ok }
    end
  end

  # method append
  def append
    if params.has_key?(:assignee)
      if params[:assignee].is_a?(Hash)
        user_ids = params[:assignee][:user_ids] || [params[:assignee][:user_id]]
        #puts user_ids
        @users = User.active.visible.where(:id => user_ids).to_a
      end
    elsif params.has_key?(:current_selected_assignees)
      user_ids = params[:current_selected_assignees]
      #@users = User.active.visible.where(:id => user_ids).to_a
      user_ids.each do |userid|
        user = User.active.visible.where(:id => userid).to_a
        if(user)
          @user.append(user)
        end
      end
      puts @users
    end
    # if @users.blank?
    #   render :nothing => true
    # end
  end

  def destroy
    @assigned.set_assignee(User.find(params[:user_id]), false)
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
      format.api { render_api_ok }
    end
  rescue ActiveRecord::RecordNotFound
    render_404
  end

  # method: autocomplete_for_user
  def autocomplete_for_user
    @users = users_for_assignees
    render :layout => false
  end

  def filter_assignee
    scope = nil

    if params.has_key?(:project_name)
      info                = params[:project_name]
      currentproject      = Project.visible.where("identifier = (?)", info).to_a
      project_id          = currentproject.first.id

      scope ||= User.active.joins(:members).where("#{Member.table_name}.project_id = ?", project_id).uniq
    else
      if @project.present?
        scope = @project.users
      end
    end
    users = scope.active.visible.sorted.like(params[:q]).to_a
    
    if @assigned
      users -= @assigned.assignee_users
    end

    users
  end

  def show
    @users = users_for_assignees
    respond_to do |format|
      format.html { render "autocomplete_for_user", 
                    :layout => false }
      format.js { @users}
      format.api { render_api_ok }
    end
  end

  def create_new_issue_with_assignees(user_id,
                                      project_id, tracker_id, status_id, priority_id, subject, description, category_id, 
                                      fixed_version_id, parent_issue_id, custom_fields, watcher_user_ids, is_private, estimated_hours,
                                    issue_token, start_date, due_date, done_ratio, workflows, issue_statuses, author_id, was_default_status,
                                    attachments)

    issue = nil
    if watcher_user_ids
      issue = Issue.new(
        :project_id           => project_id, 
        :tracker_id           => tracker_id, 
        :subject              => subject,
        :author_id            => author_id,
        :description          => description, 
        :status_id            => status_id, 
        :priority_id          => priority_id,
        :assigned_to_id       => user_id,
        :parent_issue_id      => parent_issue_id,
        :start_date           => start_date,
        :due_date             => due_date,
        :estimated_hours      => estimated_hours,
        :done_ratio           => done_ratio,
        :watcher_user_ids     => watcher_user_ids,
      )
    else
      issue = Issue.new(
        :project_id           => project_id, 
        :tracker_id           => tracker_id, 
        :subject              => subject,
        :author_id            => author_id,
        :description          => description, 
        :status_id            => status_id, 
        :priority_id          => priority_id,
        :assigned_to_id       => user_id,
        :parent_issue_id      => parent_issue_id,
        :start_date           => start_date,
        :due_date             => due_date,
        :estimated_hours      => estimated_hours,
        :done_ratio           => done_ratio,

      )
    end

    issue.save
    puts "end save issue"
    puts "end save attachment"
    
    #attachments
    if attachments.is_a?(Hash)
      attachments = attachments.stringify_keys
      attachments = attachments.to_a.sort {|a, b|
        if a.first.to_i > 0 && b.first.to_i > 0
          a.first.to_i <=> b.first.to_i
        elsif a.first.to_i > 0
          1
        elsif b.first.to_i > 0
          -1
        else
          a.first <=> b.first
        end
      }
      attachments = attachments.map(&:last)
    end

    if attachments.is_a?(Array)
      attachments.each do |attachment|
        before_attachment = Attachment.all
        puts "before add new"
        puts JSON.pretty_generate(JSON.parse(before_attachment.to_json))

        a = nil
        if attachment['token']
          puts "Token exist"
          token = attachment['token']
          a = Attachment.find_by_token(token)
        end

        filename = attachment[:filename]
        content_type = attachment[:content_type]
        description = attachment[:description]

        @attachment = Attachment.new
        @attachment.container_id = issue.id
        @attachment.container_type = "Issue"
        @attachment.author = User.current
        @attachment.filename = filename
        @attachment.content_type = content_type
        @attachment.description = description
        @attachment.digest = a[:digest]
        @attachment.filesize = a[:filesize]
        @attachment.disk_directory = a[:disk_directory]
        @attachment.disk_filename = a[:disk_filename]

        saved = @attachment.save

        after_attachment = Attachment.all
        puts "after add new"
        puts JSON.pretty_generate(JSON.parse(after_attachment.to_json))
        puts "end attachment"
      end
    end
    #attachment


    return
  end

  def assign_for_more_assignees
    if !params.has_key?(:issue)
      return
    end
    
    if !params.has_key?(:project_name)
      return
    end
    # for debugging
    # userslist = users_for_assignees
    # userslist = params[:issue][:assignee_user_ids] 
    # userslist.map(&:to_i)

    # puts JSON.pretty_generate(JSON.parse(@userslist.to_json))
    # puts "end of userlist"

    # @project = Project.visible
    # puts JSON.pretty_generate(JSON.parse(@project.to_json))
    # puts JSON.pretty_generate(JSON.parse(@info.to_json))
    # puts JSON.pretty_generate(JSON.parse(currentproject.to_json))
    # puts "end of project"
    # puts JSON.pretty_generate(JSON.parse(@user.to_json))
    # puts "end of user"
    # for debugging

    info                = params[:project_name]
    currentproject      = Project.visible.where("identifier = (?)", info).to_a
    project_id          = currentproject.first.id

    @user               = User.current    
    author_id           = @user.id

    issue_token         = params[:authenticity_token]
    is_private          = params[:issue][:is_private]
    tracker_id          = params[:issue][:tracker_id]
    subject             = params[:issue][:subject]
    description         = params[:issue][:description]
    status_id           = params[:issue][:status_id]
    priority_id         = params[:issue][:priority_id]
    assigned_to_id      = params[:issue][:assigned_to_id]
    parent_issue_id     = params[:issue][:parent_issue_id]
    start_date          = params[:issue][:start_date]
    due_date            = params[:issue][:due_date]
    estimated_hours     = params[:issue][:estimated_hours]
    done_ratio          = params[:issue][:done_ratio]
    was_default_status  = params[:was_default_status]

    assignee_user_ids   = nil #params[:issue][:assignee_user_ids].map(&:to_i)
    if params[:issue].has_key?(:assignee_user_ids)
      assignee_user_ids = params[:issue][:assignee_user_ids].map(&:to_i)
    end

    watcher_user_ids    = nil #params[:issue][:watcher_user_ids]
    if params[:issue].has_key?(:watcher_user_ids)
      watcher_user_ids  = params[:issue][:watcher_user_ids]
    end

    if params[:issue].has_key?(:assigned_to_id)
      if assignee_user_ids != nil
        if assignee_user_ids.include? params[:issue][:assigned_to_id].to_i
          assignee_user_ids.delete(params[:issue][:assigned_to_id].to_i)
        end
      end
    end

    # initialize data
    workflows = nil#params[:issue][:workflows]
    issue_statuses = nil#params[:issue][:issue_statuses]
    category_id = nil
    fixed_version_id = nil
    custom_fields = nil
    watcher_user_ids = nil
    # initialize data

    #attachments handling
    attachments = params[:attachments]


    #attachments
    #attachments handling

    if project_id && assignee_user_ids
      assignee_user_ids.each do |user|
        user_id = user
        response = create_new_issue_with_assignees(user_id,
                            project_id, tracker_id, status_id, priority_id, subject, description, category_id, 
                            fixed_version_id, parent_issue_id, custom_fields, watcher_user_ids, is_private, estimated_hours,
                            issue_token, start_date, due_date, done_ratio, workflows, issue_statuses, author_id, was_default_status,
                            attachments)
      end
    end
  end

  def set_assignee(assignables, user, assigning)
    assignables.each do |assignable|
      assignable.set_assignee(user, assigning)
    end
    respond_to do |format|
      format.html { redirect_to_referer_or {render :text => (assigning ? 'Assingee added.' : 'Assingee removed.'), :layout => true}}
      format.js { render :partial => 'set_assignee', :locals => {:user => user, :assigned => assignables} }
    end
  end
end

