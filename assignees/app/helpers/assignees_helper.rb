module AssigneesHelper
    def assignee_link(objects, user)
        return '' unless user && user.logged?
        objects = Array.wrap(objects)
        return '' unless objects.any?
    
        assigned = Assignee.any_assigned?(objects, user)
        css = [assignee_css(objects), assigned? ? 'icon icon-fav' : 'icon icon-fav-off'].join(' ')
        text = assigned ? l(:button_unwatch) : l(:button_watch)
        url = assign_path(
          :object_type => objects.first.class.to_s.underscore,
          :object_id => (objects.size == 1 ? objects.first.id : objects.map(&:id).sort)
        )
        method = assigned ? 'delete' : 'post'
    
        link_to text, url, :remote => true, :method => method, :class => css
    end

    def assignee_css(objects)
        objects = Array.wrap(objects)
        id = (objects.size == 1 ? objects.first.id : 'bulk')
        "#{objects.first.class.to_s.underscore}-#{id}-watcher"
    end

    def assignees_list(object)
      remove_allowed = User.current.allowed_to?("delete_#{object.class.name.underscore}_assignees".to_sym, object.project)
      content = ''.html_safe
      lis = object.watcher_users.collect do |user|
        s = ''.html_safe
        s << avatar(user, :size => "16").to_s
        s << link_to_user(user, :class => 'user')
        if remove_allowed
          url = {:controller => 'assignees',
                 :action => 'destroy',
                 :object_type => object.class.to_s.underscore,
                 :object_id => object.id,
                 :user_id => user}
          s << ' '
          s << link_to(image_tag('delete.png'), url,
                       :remote => true, :method => 'delete', :class => "delete")
        end
        content << content_tag('li', s, :class => "user-#{user.id}")
      end
      content.present? ? content_tag('ul', content, :class => 'watchers') : content
    end

    def assignees_checkboxes(object, users, checked=nil)
      puts JSON.pretty_generate(JSON.parse(params.to_json))

      users.map do |user|
        c = checked.nil? ? object.assigned_by?(user) : checked
        tag = check_box_tag 'issue[assignee_user_ids][]', user.id, c, :id => user.id
        content_tag 'label', "#{tag} #{h(user)}".html_safe,
                    :id => "issue_assignee_user_ids_#{user.id}",
                    :class => "floating"
      end.join.html_safe
  end

  def custom_check_box_tags(name, principals)
    s = ''
    puts JSON.pretty_generate(JSON.parse(params.to_json))
    if params == nil
      principals.each do |principal|
        s << "<label>#{ check_box_tag name, principal.id, false, :id => principal.id, :onclick => 'checkboxHandle(this.id)' } #{h principal}</label>\n"
      end
    elsif !params.has_key?(:issue)
      #if !params[:issue].has_key?(:assignee_user_ids)
        principals.each do |principal|
          s << "<label>#{ check_box_tag name, principal.id, false, :id => principal.id , :onclick => 'checkboxHandle(this.id)'} #{h principal}</label>\n"
        end
        s.html_safe
      #end
    elsif !params[:issue].has_key?(:assignee_user_ids)
      principals.each do |principal|
        s << "<label>#{ check_box_tag name, principal.id, false, :id => principal.id , :onclick => 'checkboxHandle(this.id)'} #{h principal}</label>\n"
      end
      s.html_safe

    # test
    # elsif params.has_key?(:q)
    #   users.map do |user|
    #     c = checked.nil? ? object.assigned_by?(user) : checked
    #     tag = check_box_tag 'issue[assignee_user_ids][]', user.id, c, :id => nil
    #     content_tag 'label', "#{tag} #{h(user)}".html_safe,
    #                 :id => "issue_assignee_user_ids_#{user.id}",
    #                 :class => "floating"
    #   end.join.html_safe 
    # test

    else
      assignee_user_ids = params[:issue][:assignee_user_ids].map(&:to_i)
      principals.each do |principal|
        set = false
        assignee_user_ids.each do |assignee|
          if assignee == principal.id
            s << "<label>#{ check_box_tag name, principal.id, true, :id => principal.id , :onclick => 'checkboxHandle(this.id)'} #{h principal}</label>\n"
            set = true
            break
          end
        end
        if set == false
          s << "<label>#{ check_box_tag name, principal.id, false, :id => principal.id , :onclick => 'checkboxHandle(this.id)'} #{h principal}</label>\n"
        end
      end
      s.html_safe
    end
    # principals.each do |principal|
    #   s << "<label>#{ check_box_tag name, principal.id, false, :id => nil } #{h principal}</label>\n"
    # end
    # s.html_safe
  end
end
