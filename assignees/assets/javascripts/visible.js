$(document).ready(function() {      
    let searchParams = window.location.href.replace(window.location.origin + '/', "")
    let a = searchParams.trim().split("/")
    if (a[0].localeCompare("issues") == 0){
        document.getElementById("assignees_form").style.visibility = "collapse";
        document.getElementById("assignees_form").style.display = "none";
    }
    else
    {
        document.getElementById("assignees_form").style.visibility = "visible";
    }
})