$(document).ready(function() {      
    $( "#issue-form" ).submit(function( event ) {
        var a = $(this).data("allow")
        if(!a)
        {
            let searchParams = window.location.href.replace(window.location.origin + '/', "")
            let a = searchParams.trim().split("/")
            //console.log(a[1]) // for testing
            var issue = $('#issue-form').serialize()
            
            //event.preventDefault(); // uncomment this line when you want to pause the submittion of "issue-form"
            $.ajax({
                type: "post",
                url: "/assignees/assign_for_more_assignees",
                data: issue + '&project_name=' + a[1],
            })
        }
        $(this).data("allow", true)
        $(this).submit() // comment this line when you want to pause the submittion of "issue-form"
    })
})