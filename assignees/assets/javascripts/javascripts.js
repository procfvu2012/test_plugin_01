var current_selected_assignees = []

function observeSearchfield(fieldId, targetId, url) {
  $('#'+fieldId).each(function() {
    var $this = $(this);
    $this.addClass('autocomplete');
    $this.attr('data-value-was', $this.val());
    var check = function() {
      var val = $this.val();
      if ($this.attr('data-value-was') != val){
        $this.attr('data-value-was', val);

        // test ntvu
        let searchParams = window.location.href.replace(window.location.origin + '/', "")
        let a = searchParams.trim().split("/")
        var issue = $('#issue-form').serialize()
        
        // retrieve all the list of id of checkbox that is being show on the screen
        var assignee_search = document.getElementById("users_for_assignee").children
        var id_list = []

        for (i = 0; i <= assignee_search.length - 1; i++) {
          //id_list.push(assignee_search[i].id)
          
          var inside_label_info = assignee_search[i].children

          id_list.push(inside_label_info[0].id)
        }

        console.log(id_list)
        // retrieve all the list of id of checkbox that is being show on the screen

        // handling for selected user
        
        for(i = 0; i <= id_list.length - 1; i++)
        {
          var checkbox = document.getElementById(id_list[i])
          if(!checkbox) return
          if (checkbox.checked)
          {
            current_selected_assignees.push(parseInt(id_list[i]))
          }
        }

        console.log(current_selected_assignees)
        



        //var assignee_search_data = assignee_search.innerHTML
        // test 

        $.ajax({
          url: url,
          type: 'get',
          data: issue + '&project_name=' + a[1] + '&project_id=' + a[1] + '&q=' + $this.val() + '&current_selected_assignees=' + current_selected_assignees,
          success: function(data){ if(targetId) $('#'+targetId).html(data); },
          beforeSend: function(){ $this.addClass('ajax-loading'); },
          complete: function(){ $this.removeClass('ajax-loading'); }
        });
      }
    };
    var reset = function() {
      if (timer) {
        clearInterval(timer);
        timer = setInterval(check, 300);
      }
    };
    var timer = setInterval(check, 300);
    $this.bind('keyup click mousemove', reset);
  });
}

function searchbox_processing(){
  var searchbox = document.getElementById('user_search')
  searchbox.addEventListener('input', (event) => {
    console.log("input")
  });

  let searchParams = window.location.href.replace(window.location.origin + '/', "")
  let a = searchParams.trim().split("/")
  //console.log(a[1]) // for testing
  var issue = $('#issue-form').serialize()
  var assignee_search = document.getElementById("users_for_assignee").children
  var id_list = []

  // for (i = 0; i <= assignee_search.length - 1; i++) {
  //   //id_list.push(assignee_search[i].id)
  //   var label = document.getElementById(assignee_search[i].id).children
  //   id_list.push(label.id)
  // }



  // $.ajax({
  //     type: "get",
  //     url: "/assignees/new",
  //     data: issue + '&project_name=' + a[1] + '&project_id=' + a[1],
  // })
}

// ntvu
function checkboxHandle(checkboxId){
  var checkbox = document.getElementById(checkboxId)
  if(!checkbox) return
  if (checkbox.checked)
  {
    checkbox.checked = true
  }
  else
  {
    checkbox.checked = false
  }  
}
// ntvu

function showModal(id, width, title) {
  var el = $('#'+id).first();
  if (el.length === 0 || el.is(':visible')) {return;}
  if (!title) title = el.find('h3.title').text();
  // moves existing modals behind the transparent background
  $(".modal").zIndex(99);
  el.dialog({
    width: width,
    modal: true,
    resizable: false,
    dialogClass: 'modal',
    title: title
  }).on('dialogclose', function(){
    $(".modal").zIndex(101);
  });
  el.find("input[type=text], input[type=submit]").first().focus();
}

function hideModal(el) {
  if (current_selected_assignees.length > 0)
  {
    console.log(current_selected_assignees)
  }
  var assignee_search = document.getElementById("users_for_assignee").children
  var id_list = []

  for (i = 0; i <= assignee_search.length - 1; i++) {
    //id_list.push(assignee_search[i].id)
    
    var inside_label_info = assignee_search[i].children

    id_list.push(inside_label_info[0].id)
  }

  for(i = 0; i <= id_list.length - 1; i++)
  {
    var checkbox = document.getElementById(id_list[i])
    if(!checkbox) return
    if (checkbox.checked)
    {
      if(!current_selected_assignees.includes(id_list[i]))
      {
        current_selected_assignees.push(parseInt(id_list[i]))
      }
    }
    else
    {
      if(current_selected_assignees.includes(id_list[i]))
      {
        current_selected_assignees.push(id_list[i])
        const index = current_selected_assignees.indexOf(id_list[i])
        if(index > -1)
        {
          current_selected_assignees.splice(index, 1)
        }
      }
    }
  }

  let searchParams = window.location.href.replace(window.location.origin + '/', "")
  let a = searchParams.trim().split("/")
  var issue = $('#issue-form').serialize()


  var modal;
  if (el) {
    modal = $(el).parents('.ui-dialog-content');
  } else {
    modal = $('#ajax-modal');
  }
  $.ajax({
    url: '/assignees/append',
    type: 'post',
    data: issue + '&project_name=' + a[1] + '&project_id=' + a[1] + '&current_selected_assignees=' + current_selected_assignees,
  });
  modal.dialog("close");
}
