# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

resources :assignees

post 'assignees/assign', :to => 'assignees#assign', :as => 'assign'
delete 'assignees/assign', :to => 'assignees#unassign'
get 'assignees/new', :to => 'assignees#new'
post 'assignees', :to => 'assignees#create'
post 'assignees/append', :to => 'assignees#append'
delete 'assignees', :to => 'assignees#destroy'
get 'assignees/autocomplete_for_user', :to => 'assignees#autocomplete_for_user'
post 'assignees/assign_for_more_assignees', :to => 'assignees#assign_for_more_assignees'

post 'assignees/new', :to => 'assignees#new'
post 'assignees/test', :to => 'assignees#test'
# ActionController::Routing::Routes.draw do |map|
#     map.connect 'assignees/:action', :controller => 'assignees'
# end