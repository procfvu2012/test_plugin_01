require_dependency 'assignees_hook_listener'

Redmine::Plugin.register :assignees do
  name 'More Assignees'
  author 'ISB VietNam'
  description 'Create one task, assign for more users.'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'
end
