class PollsHookListener < Redmine::Hook::ViewListener
    render_on :view_issues_form_details_bottom, partial: "assignees/more_assignees"
end
   